package com.itheima.heima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @Auth: zhuan
 * @Desc: 黑马测试项目-V1.1
 * @Date: 2023/3/22 8:49
 */
@SpringBootApplication
public class HeimaApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeimaApplication.class, args);
    }

}
